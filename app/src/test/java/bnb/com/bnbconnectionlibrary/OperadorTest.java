package bnb.com.bnbconnectionlibrary;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class OperadorTest {

    Operador operador;

    @Before
    public void setUp(){
        operador = new Operador();
    }

    @Test
    public void suma() {
        assertEquals("4",operador.suma("3","1"));
    }
}