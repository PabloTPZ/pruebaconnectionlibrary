package bnb.com.bnbconnectionlibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.controllers.ObservableOTPController;
import bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.controllers.ObserverOTPController;
import bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.models.SendDataOTP;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

    }
}
