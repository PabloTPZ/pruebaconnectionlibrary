package bnb.com.bnbconnection;

public interface BNBCallback<T> {
    void bnbSuccessful(T response, String log);
    void bnbError(String error, String log);
}
