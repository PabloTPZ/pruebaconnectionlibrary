package bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.models.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseOTP {

    @SerializedName("hotpCode")
    @Expose
    private String hotpCode;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private String code;

    public String getHotpCode() {
        return hotpCode;
    }

    public void setHotpCode(String hotpCode) {
        this.hotpCode = hotpCode;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
