package bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.controllers;

import android.util.Log;


import com.google.gson.Gson;

import java.util.Observable;
import java.util.Observer;

import bnb.com.bnbconnection.bnbconnectionauthentication.APIServiceAuthentication;
import bnb.com.bnbconnection.bnbconnectionauthentication.BNBConnectionUtilsAuthentication;
import bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.models.Response.ResponseOTP;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ObserverOTPController implements Observer {

    private APIServiceAuthentication apiServiceAuthentication;

    @Override
    public void update(Observable observable, Object o) throws NullPointerException {
        final ObservableOTPController observableOTPController = (ObservableOTPController) observable;

        if (observableOTPController.getSendDataOTP() != null){
            observableOTPController.deleteObserver(this);
            apiServiceAuthentication = BNBConnectionUtilsAuthentication.getAPIServiceOTAPIServiceAuthentication();
            Call<ResponseOTP> call = apiServiceAuthentication.sendDataOTP(observableOTPController.getSendDataOTP());
            call.enqueue(new Callback<ResponseOTP>() {
                @Override
                public void onResponse(Call<ResponseOTP> call, Response<ResponseOTP> response) {
                    ResponseOTP responseOTP = (ResponseOTP) response.body();
                    Gson gson = new Gson();
                    Log.d("prueba", "onResponse: " + gson.toJson(responseOTP));
                    if (responseOTP.getSuccess()) {
                        observableOTPController.getBnbCallback().bnbSuccessful(response.body(), "Generacion de HOTP correcta");
                    } else {
                        observableOTPController.getBnbCallback().bnbError(response.body().getMessage(), "Error datos incorectos obtención HTOP");
                    }
                }

                @Override
                public void onFailure(Call<ResponseOTP> call, Throwable t) {
                    observableOTPController.getBnbCallback().bnbError(t.getMessage(), "Error Petición HTOP");
                }
            });
        }
    }
}
