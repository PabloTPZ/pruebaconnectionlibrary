package bnb.com.bnbconnection.bnbconnectionauthentication;

import bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.models.Response.ResponseOTP;
import bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.models.SendDataOTP;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIServiceAuthentication {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("Auth/GetHotpCode")
    Call<ResponseOTP> sendDataOTP(@Body SendDataOTP body);
}
