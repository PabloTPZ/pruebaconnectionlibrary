package bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.controllers;

import java.util.Observable;

import bnb.com.bnbconnection.BNBCallback;
import bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.models.SendDataOTP;

public class ObservableOTPController extends Observable {

    private SendDataOTP sendDataOTP;
    private BNBCallback bnbCallback;

    public ObservableOTPController(){
    }

    public SendDataOTP getSendDataOTP() {
        return sendDataOTP;
    }

    public BNBCallback getBnbCallback() {
        return bnbCallback;
    }

    public void setReady(SendDataOTP sendDataOTP,
                         BNBCallback bnbCallback){
        this.sendDataOTP = sendDataOTP;
        this.bnbCallback = bnbCallback;
        setChanged();
        notifyObservers();
    }
}
