package bnb.com.bnbconnection.bnbconnectionauthentication;
import bnb.com.bnbconnection.BNBConnectionConstants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BNBConnectionUtilsAuthentication {

    private static Retrofit retrofit = null;

    public static APIServiceAuthentication getAPIServiceOTAPIServiceAuthentication() {
        return getClient(BNBConnectionConstants.URLBASESERVERPRUEBA).create(APIServiceAuthentication.class);
    }

    private static Retrofit getClient(String baseURL){
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        return retrofit;
    }
}
