
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetalleRegistro {

    @SerializedName("TipoActivacion")
    @Expose
    private Integer tipoActivacion;
    @SerializedName("EnvioLinkPorCorreo")
    @Expose
    private Boolean envioLinkPorCorreo;
    @SerializedName("Telefono")
    @Expose
    private Object telefono;
    @SerializedName("Correo")
    @Expose
    private Object correo;
    @SerializedName("CodigoPais")
    @Expose
    private Object codigoPais;
    @SerializedName("FechaSolicitud")
    @Expose
    private String fechaSolicitud;
    @SerializedName("CanalRegistro")
    @Expose
    private Integer canalRegistro;
    @SerializedName("SubCanalRegistro")
    @Expose
    private Integer subCanalRegistro;

    public Integer getTipoActivacion() {
        return tipoActivacion;
    }

    public void setTipoActivacion(Integer tipoActivacion) {
        this.tipoActivacion = tipoActivacion;
    }

    public Boolean getEnvioLinkPorCorreo() {
        return envioLinkPorCorreo;
    }

    public void setEnvioLinkPorCorreo(Boolean envioLinkPorCorreo) {
        this.envioLinkPorCorreo = envioLinkPorCorreo;
    }

    public Object getTelefono() {
        return telefono;
    }

    public void setTelefono(Object telefono) {
        this.telefono = telefono;
    }

    public Object getCorreo() {
        return correo;
    }

    public void setCorreo(Object correo) {
        this.correo = correo;
    }

    public Object getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(Object codigoPais) {
        this.codigoPais = codigoPais;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public Integer getCanalRegistro() {
        return canalRegistro;
    }

    public void setCanalRegistro(Integer canalRegistro) {
        this.canalRegistro = canalRegistro;
    }

    public Integer getSubCanalRegistro() {
        return subCanalRegistro;
    }

    public void setSubCanalRegistro(Integer subCanalRegistro) {
        this.subCanalRegistro = subCanalRegistro;
    }

}
