
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.questions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Factores {

    @SerializedName("FactoresTransaccion")
    @Expose
    private FactoresTransaccion factoresTransaccion;
    @SerializedName("Mensaje")
    @Expose
    private Object mensaje;
    @SerializedName("Correcto")
    @Expose
    private Boolean correcto;

    public FactoresTransaccion getFactoresTransaccion() {
        return factoresTransaccion;
    }

    public void setFactoresTransaccion(FactoresTransaccion factoresTransaccion) {
        this.factoresTransaccion = factoresTransaccion;
    }

    public Object getMensaje() {
        return mensaje;
    }

    public void setMensaje(Object mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getCorrecto() {
        return correcto;
    }

    public void setCorrecto(Boolean correcto) {
        this.correcto = correcto;
    }

}
