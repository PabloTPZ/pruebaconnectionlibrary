package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionpassword.controllers;

import java.util.Observable;

import bnb.com.bnbconnection.BNBCallback;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionpassword.models.request.LoginRequestPassword;

public class ObservablePasswordControllers extends Observable{

    private LoginRequestPassword loginRequestPassword;
    private BNBCallback bnbCallback;

    public ObservablePasswordControllers(){
    }

    public LoginRequestPassword getLoginRequestPassword() {
        return loginRequestPassword;
    }

    public BNBCallback getBnbCallback() {
        return bnbCallback;
    }

    public void setReady(LoginRequestPassword loginRequestPassword,
                         BNBCallback bnbCallback){
        this.loginRequestPassword = loginRequestPassword;
        this.bnbCallback = bnbCallback;

        setChanged();
        notifyObservers();
    }
}
