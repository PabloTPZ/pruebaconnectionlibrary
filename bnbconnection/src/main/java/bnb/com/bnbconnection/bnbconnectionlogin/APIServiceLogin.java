package bnb.com.bnbconnection.bnbconnectionlogin;

import bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.models.Response.ResponseOTP;
import bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.models.SendDataOTP;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionpassword.models.request.LoginRequestPassword;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionpassword.models.response.LoginResponsePassword;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionuser.models.response.LoginResponseUser;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionuser.models.request.LoginResquestUser;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.response.QuestionsResponse;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionbeforequestions.models.response.QuestionsResponseSend;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.request.QuestionsResquest;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionbeforequestions.models.request.QuestionsSend;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIServiceLogin {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("IniciarSesionMobile/PostIdentificadorV2")
    Call<LoginResponseUser> userPost(@Body LoginResquestUser body);

    @POST("IniciarSesionMobile/PostIniciarClave")
    Call<LoginResponsePassword> passwordPost(@Body LoginRequestPassword body);

    @POST("SeguridadMobile/PostFactoresDatosSeguridad")
    Call<QuestionsResponse> questionsPost(@Body QuestionsResquest body);

    @POST("UsuarioMobile/PostGuardarDatosSeguridad")
    Call<QuestionsResponseSend> sendQuestiontPost(@Body QuestionsSend body);
}
