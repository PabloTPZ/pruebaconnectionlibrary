
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pregunta_ {

    @SerializedName("Numero")
    @Expose
    private Integer numero;
    @SerializedName("Enunciado")
    @Expose
    private String enunciado;
    @SerializedName("Respuesta")
    @Expose
    private String respuesta;

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

}
