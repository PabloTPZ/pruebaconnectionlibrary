
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SoftToken {

    @SerializedName("Numero")
    @Expose
    private Object numero;
    @SerializedName("TokenSet")
    @Expose
    private Object tokenSet;
    @SerializedName("DetalleRegistro")
    @Expose
    private DetalleRegistro detalleRegistro;

    public Object getNumero() {
        return numero;
    }

    public void setNumero(Object numero) {
        this.numero = numero;
    }

    public Object getTokenSet() {
        return tokenSet;
    }

    public void setTokenSet(Object tokenSet) {
        this.tokenSet = tokenSet;
    }

    public DetalleRegistro getDetalleRegistro() {
        return detalleRegistro;
    }

    public void setDetalleRegistro(DetalleRegistro detalleRegistro) {
        this.detalleRegistro = detalleRegistro;
    }

}
