
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.questions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DesafioSMS {

    @SerializedName("Situacion")
    @Expose
    private Integer situacion;
    @SerializedName("Numero")
    @Expose
    private Integer numero;
    @SerializedName("Mensaje")
    @Expose
    private Object mensaje;
    @SerializedName("Correcto")
    @Expose
    private Boolean correcto;

    public Integer getSituacion() {
        return situacion;
    }

    public void setSituacion(Integer situacion) {
        this.situacion = situacion;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Object getMensaje() {
        return mensaje;
    }

    public void setMensaje(Object mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getCorrecto() {
        return correcto;
    }

    public void setCorrecto(Boolean correcto) {
        this.correcto = correcto;
    }

}
