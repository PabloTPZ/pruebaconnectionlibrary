package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionuser.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.auth.Factore;

public class LoginResponseUser {

    @SerializedName("Usuario")
    @Expose
    private Long usuario;
    @SerializedName("Estado")
    @Expose
    private int estado;
    @SerializedName("Factores")
    @Expose
    private List<Factore> factores = null;
    @SerializedName("DebeCambiarPreguntas")
    @Expose
    private Boolean debeCambiarPreguntas;
    @SerializedName("Imagen")
    @Expose
    private Imagen imagen;
    @SerializedName("SituacionEjecucion")
    @Expose
    private int situacionEjecucion;
    @SerializedName("Correcto")
    @Expose
    private Boolean correcto;
    @SerializedName("Mensaje")
    @Expose
    private String mensaje;
    @SerializedName("TipoError")
    @Expose
    private Integer tipoError;
    @SerializedName("ActualizarVersion")
    @Expose
    private Boolean actualizarVersion;
    @SerializedName("ExisteNuevaVersion")
    @Expose
    private Boolean existeNuevaVersion;

    public Long getUsuario() {
        return usuario;
    }

    public void setUsuario(Long usuario) {
        this.usuario = usuario;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public List<Factore> getFactores() {
        return factores;
    }

    public void setFactores(List<Factore> factores) {
        this.factores = factores;
    }

    public Boolean getDebeCambiarPreguntas() {
        return debeCambiarPreguntas;
    }

    public void setDebeCambiarPreguntas(Boolean debeCambiarPreguntas) {
        this.debeCambiarPreguntas = debeCambiarPreguntas;
    }

    public Imagen getImagen() {
        return imagen;
    }

    public void setImagen(Imagen imagen) {
        this.imagen = imagen;
    }

    public Integer getSituacionEjecucion() {
        return situacionEjecucion;
    }

    public void setSituacionEjecucion(Integer situacionEjecucion) {
        this.situacionEjecucion = situacionEjecucion;
    }

    public Boolean getCorrecto() {
        return correcto;
    }

    public void setCorrecto(Boolean correcto) {
        this.correcto = correcto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Integer getTipoError() {
        return tipoError;
    }

    public void setTipoError(Integer tipoError) {
        this.tipoError = tipoError;
    }

    public Boolean getActualizarVersion() {
        return actualizarVersion;
    }

    public void setActualizarVersion(Boolean actualizarVersion) {
        this.actualizarVersion = actualizarVersion;
    }

    public Boolean getExisteNuevaVersion() {
        return existeNuevaVersion;
    }

    public void setExisteNuevaVersion(Boolean existeNuevaVersion) {
        this.existeNuevaVersion = existeNuevaVersion;
    }

}
