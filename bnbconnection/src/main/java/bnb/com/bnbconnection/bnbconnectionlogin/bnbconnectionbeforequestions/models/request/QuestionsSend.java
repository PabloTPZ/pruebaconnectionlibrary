package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionbeforequestions.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.request.Cliente;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.request.Factores;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.request.Pregunta;

public class QuestionsSend {


    @SerializedName("ModificarClave")
    @Expose
    private Boolean modificarClave;
    @SerializedName("Clientes")
    @Expose
    private List<Cliente> clientes = null;
    @SerializedName("IdentificadorFactor")
    @Expose
    private Integer identificadorFactor;
    @SerializedName("ClaveActual")
    @Expose
    private String claveActual;
    @SerializedName("Origen")
    @Expose
    private String origen;
    @SerializedName("ModificarIdentificador")
    @Expose
    private Boolean modificarIdentificador;
    @SerializedName("Identificador")
    @Expose
    private String identificador;
    @SerializedName("RegistrarPreguntas")
    @Expose
    private Boolean registrarPreguntas;
    @SerializedName("Factores")
    @Expose
    private Factores factores;
    @SerializedName("CodigoUsuario")
    @Expose
    private Long codigoUsuario;
    @SerializedName("Preguntas")
    @Expose
    private List<Pregunta> preguntas = null;
    @SerializedName("RegistrarEquipo")
    @Expose
    private Boolean registrarEquipo;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("NombreOrigen")
    @Expose
    private String nombreOrigen;

    public Boolean getModificarClave() {
        return modificarClave;
    }

    public void setModificarClave(Boolean modificarClave) {
        this.modificarClave = modificarClave;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public Integer getIdentificadorFactor() {
        return identificadorFactor;
    }

    public void setIdentificadorFactor(Integer identificadorFactor) {
        this.identificadorFactor = identificadorFactor;
    }

    public String getClaveActual() {
        return claveActual;
    }

    public void setClaveActual(String claveActual) {
        this.claveActual = claveActual;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Boolean getModificarIdentificador() {
        return modificarIdentificador;
    }

    public void setModificarIdentificador(Boolean modificarIdentificador) {
        this.modificarIdentificador = modificarIdentificador;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public Boolean getRegistrarPreguntas() {
        return registrarPreguntas;
    }

    public void setRegistrarPreguntas(Boolean registrarPreguntas) {
        this.registrarPreguntas = registrarPreguntas;
    }

    public Factores getFactores() {
        return factores;
    }

    public void setFactores(Factores factores) {
        this.factores = factores;
    }

    public Long getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(Long codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public List<Pregunta> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }

    public Boolean getRegistrarEquipo() {
        return registrarEquipo;
    }

    public void setRegistrarEquipo(Boolean registrarEquipo) {
        this.registrarEquipo = registrarEquipo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNombreOrigen() {
        return nombreOrigen;
    }

    public void setNombreOrigen(String nombreOrigen) {
        this.nombreOrigen = nombreOrigen;
    }


}
