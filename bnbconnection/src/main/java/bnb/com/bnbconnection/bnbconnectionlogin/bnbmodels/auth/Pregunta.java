
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.auth;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pregunta {

    @SerializedName("Numero")
    @Expose
    private Integer numero;
    @SerializedName("Preguntas")
    @Expose
    private List<Pregunta_> preguntas = null;

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public List<Pregunta_> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(List<Pregunta_> preguntas) {
        this.preguntas = preguntas;
    }

}
