
package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.request;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FactoresTransaccion {


    @SerializedName("CodigoUsuario")
    @Expose
    private Long codigoUsuario;

    @SerializedName("DesafioPregunta")
    @Expose
    private DesafioPregunta desafioPregunta = null;

    @SerializedName("ListaFactores")
    @Expose
    private List<ListaFactore> listaFactores = null;

    @SerializedName("Situacion")
    @Expose
    private Integer situacion;

    @SerializedName("TienePregunta")
    @Expose
    private boolean tienePregunta;

    @SerializedName("TieneSMS")
    @Expose
    private boolean tieneSms;

    @SerializedName("TieneSoftToken")
    @Expose
    private boolean tieneSoftToken;

    @SerializedName("TieneTarjeta")
    @Expose
    private boolean tieneTarjeta;

    @SerializedName("TieneToken")
    @Expose
    private boolean tieneToken;



    public DesafioPregunta getDesafioPregunta() {
        return desafioPregunta;
    }

    public void setDesafioPregunta(DesafioPregunta desafioPregunta) {
        this.desafioPregunta = desafioPregunta;
    }

    public Long getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(Long codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public Integer getSituacion() {
        return situacion;
    }

    public void setSituacion(Integer situacion) {
        this.situacion = situacion;
    }

    public boolean isTienePregunta() {
        return tienePregunta;
    }

    public void setTienePregunta(boolean tienePregunta) {
        this.tienePregunta = tienePregunta;
    }

    public boolean isTieneSms() {
        return tieneSms;
    }

    public void setTieneSms(boolean tieneSms) {
        this.tieneSms = tieneSms;
    }

    public boolean isTieneSoftToken() {
        return tieneSoftToken;
    }

    public void setTieneSoftToken(boolean tieneSoftToken) {
        this.tieneSoftToken = tieneSoftToken;
    }

    public boolean isTieneTarjeta() {
        return tieneTarjeta;
    }

    public void setTieneTarjeta(boolean tieneTarjeta) {
        this.tieneTarjeta = tieneTarjeta;
    }

    public boolean isTieneToken() {
        return tieneToken;
    }

    public void setTieneToken(boolean tieneToken) {
        this.tieneToken = tieneToken;
    }

    public List<ListaFactore> getListaFactores() {
        return listaFactores;
    }

    public void setListaFactores(List<ListaFactore> listaFactores) {
        this.listaFactores = listaFactores;
    }
}
