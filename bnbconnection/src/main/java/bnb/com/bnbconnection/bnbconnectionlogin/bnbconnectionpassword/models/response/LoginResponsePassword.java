package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionpassword.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.request.Cliente;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.auth.Factore;

public class LoginResponsePassword {

    @SerializedName("Dato")
    @Expose
    private Object dato;
    @SerializedName("Usuario")
    @Expose
    private Long usuario;
    @SerializedName("Estado")
    @Expose
    private Integer estado;
    @SerializedName("FechaUltimoAcceso")
    @Expose
    private String fechaUltimoAcceso;
    @SerializedName("Clientes")
    @Expose
    private List<Cliente> clientes = null;
    @SerializedName("Factores")
    @Expose
    private List<Factore> factores = null;
    @SerializedName("DebeCambiarPreguntas")
    @Expose
    private Boolean debeCambiarPreguntas;
    @SerializedName("DebeCambiarClave")
    @Expose
    private Boolean debeCambiarClave;
    @SerializedName("DebeCambiarIdentificador")
    @Expose
    private Boolean debeCambiarIdentificador;
    @SerializedName("DebeRegistrarEquipo")
    @Expose
    private Boolean debeRegistrarEquipo;
    @SerializedName("SituacionEjecucion")
    @Expose
    private Integer situacionEjecucion;
    @SerializedName("Correcto")
    @Expose
    private Boolean correcto;
    @SerializedName("Mensaje")
    @Expose
    private String mensaje;
    @SerializedName("TipoError")
    @Expose
    private Integer tipoError;

    public Object getDato() {
        return dato;
    }

    public void setDato(Object dato) {
        this.dato = dato;
    }

    public Long getUsuario() {
        return usuario;
    }

    public void setUsuario(Long usuario) {
        this.usuario = usuario;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getFechaUltimoAcceso() {
        return fechaUltimoAcceso;
    }

    public void setFechaUltimoAcceso(String fechaUltimoAcceso) {
        this.fechaUltimoAcceso = fechaUltimoAcceso;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public List<Factore> getFactores() {
        return factores;
    }

    public void setFactores(List<Factore> factores) {
        this.factores = factores;
    }

    public Boolean getDebeCambiarPreguntas() {
        return debeCambiarPreguntas;
    }

    public void setDebeCambiarPreguntas(Boolean debeCambiarPreguntas) {
        this.debeCambiarPreguntas = debeCambiarPreguntas;
    }

    public Boolean getDebeCambiarClave() {
        return debeCambiarClave;
    }

    public void setDebeCambiarClave(Boolean debeCambiarClave) {
        this.debeCambiarClave = debeCambiarClave;
    }

    public Boolean getDebeCambiarIdentificador() {
        return debeCambiarIdentificador;
    }

    public void setDebeCambiarIdentificador(Boolean debeCambiarIdentificador) {
        this.debeCambiarIdentificador = debeCambiarIdentificador;
    }

    public Boolean getDebeRegistrarEquipo() {
        return debeRegistrarEquipo;
    }

    public void setDebeRegistrarEquipo(Boolean debeRegistrarEquipo) {
        this.debeRegistrarEquipo = debeRegistrarEquipo;
    }

    public Integer getSituacionEjecucion() {
        return situacionEjecucion;
    }

    public void setSituacionEjecucion(Integer situacionEjecucion) {
        this.situacionEjecucion = situacionEjecucion;
    }

    public Boolean getCorrecto() {
        return correcto;
    }

    public void setCorrecto(Boolean correcto) {
        this.correcto = correcto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Integer getTipoError() {
        return tipoError;
    }

    public void setTipoError(Integer tipoError) {
        this.tipoError = tipoError;
    }

}
