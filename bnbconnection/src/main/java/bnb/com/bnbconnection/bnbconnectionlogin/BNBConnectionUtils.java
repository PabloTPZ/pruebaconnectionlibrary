package bnb.com.bnbconnection.bnbconnectionlogin;
import bnb.com.bnbconnection.BNBConnectionConstants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BNBConnectionUtils {

    private static Retrofit retrofit = null;

    //retrofit
    public static APIServiceLogin getAPIService() {
        return getClient(BNBConnectionConstants.URLBaseServer).create(APIServiceLogin.class);
    }

    public static APIServiceLogin getAPIServiceOTP() {
        return getClient(BNBConnectionConstants.URLBASESERVERPRUEBA).create(APIServiceLogin.class);
    }

    private static Retrofit getClient(String baseURL){

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        return retrofit;
    }
}
