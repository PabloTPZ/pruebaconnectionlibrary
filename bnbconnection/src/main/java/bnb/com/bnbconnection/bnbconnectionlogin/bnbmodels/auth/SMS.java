
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SMS {

    @SerializedName("Numero")
    @Expose
    private Object numero;
    @SerializedName("CodigoPais")
    @Expose
    private Object codigoPais;

    public Object getNumero() {
        return numero;
    }

    public void setNumero(Object numero) {
        this.numero = numero;
    }

    public Object getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(Object codigoPais) {
        this.codigoPais = codigoPais;
    }

}
