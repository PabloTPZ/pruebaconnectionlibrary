
package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Factores {

    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("CodigoCliente")
    @Expose
    private Integer codigoCliente;
    @SerializedName("Origen")
    @Expose
    private String origen;
    @SerializedName("Correcto")
    @Expose
    private Boolean correcto;
    @SerializedName("Mensaje")
    @Expose
    private String mensaje;
    @SerializedName("FactoresTransaccion")
    @Expose
    private FactoresTransaccion factoresTransaccion;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(Integer codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public Boolean getCorrecto() {
        return correcto;
    }

    public void setCorrecto(Boolean correcto) {
        this.correcto = correcto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public FactoresTransaccion getFactoresTransaccion() {
        return factoresTransaccion;
    }

    public void setFactoresTransaccion(FactoresTransaccion factoresTransaccion) {
        this.factoresTransaccion = factoresTransaccion;
    }

}
