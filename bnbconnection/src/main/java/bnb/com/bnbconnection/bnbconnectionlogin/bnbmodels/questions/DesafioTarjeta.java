
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.questions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DesafioTarjeta {

    @SerializedName("Situacion")
    @Expose
    private Integer situacion;
    @SerializedName("Numero")
    @Expose
    private Integer numero;
    @SerializedName("Desafio1")
    @Expose
    private Object desafio1;
    @SerializedName("DesafioRespuesta1")
    @Expose
    private Object desafioRespuesta1;
    @SerializedName("Desafio2")
    @Expose
    private Object desafio2;
    @SerializedName("DesafioRespuesta2")
    @Expose
    private Object desafioRespuesta2;
    @SerializedName("Desafio3")
    @Expose
    private Object desafio3;
    @SerializedName("DesafioRespuesta3")
    @Expose
    private Object desafioRespuesta3;
    @SerializedName("Mensaje")
    @Expose
    private Object mensaje;
    @SerializedName("Correcto")
    @Expose
    private Boolean correcto;

    public Integer getSituacion() {
        return situacion;
    }

    public void setSituacion(Integer situacion) {
        this.situacion = situacion;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Object getDesafio1() {
        return desafio1;
    }

    public void setDesafio1(Object desafio1) {
        this.desafio1 = desafio1;
    }

    public Object getDesafioRespuesta1() {
        return desafioRespuesta1;
    }

    public void setDesafioRespuesta1(Object desafioRespuesta1) {
        this.desafioRespuesta1 = desafioRespuesta1;
    }

    public Object getDesafio2() {
        return desafio2;
    }

    public void setDesafio2(Object desafio2) {
        this.desafio2 = desafio2;
    }

    public Object getDesafioRespuesta2() {
        return desafioRespuesta2;
    }

    public void setDesafioRespuesta2(Object desafioRespuesta2) {
        this.desafioRespuesta2 = desafioRespuesta2;
    }

    public Object getDesafio3() {
        return desafio3;
    }

    public void setDesafio3(Object desafio3) {
        this.desafio3 = desafio3;
    }

    public Object getDesafioRespuesta3() {
        return desafioRespuesta3;
    }

    public void setDesafioRespuesta3(Object desafioRespuesta3) {
        this.desafioRespuesta3 = desafioRespuesta3;
    }

    public Object getMensaje() {
        return mensaje;
    }

    public void setMensaje(Object mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getCorrecto() {
        return correcto;
    }

    public void setCorrecto(Boolean correcto) {
        this.correcto = correcto;
    }

}
