package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionbeforequestions.controllers;

import java.util.Observable;
import java.util.Observer;

import bnb.com.bnbconnection.bnbconnectionlogin.BNBConnectionUtils;
import bnb.com.bnbconnection.bnbconnectionlogin.APIServiceLogin;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.response.QuestionsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ObserverBeforeQuestionsControllers implements Observer {

    private APIServiceLogin apiServiceLogin;

    @Override
    public void update(Observable o, Object arg) {

        final ObservableBeforeQuestionsControllers observableBeforeQuestionsControllers = (ObservableBeforeQuestionsControllers) o;
        if (observableBeforeQuestionsControllers != null){
            apiServiceLogin = BNBConnectionUtils.getAPIService();

            Call<QuestionsResponse> call = apiServiceLogin.questionsPost(observableBeforeQuestionsControllers.getQuestionsResquest());
            call.enqueue(new Callback<QuestionsResponse>() {
                @Override
                public void onResponse(Call<QuestionsResponse> call, Response<QuestionsResponse> response) {
                    observableBeforeQuestionsControllers.getBnbCallback().bnbSuccessful(response.body(), "Devolucion pregunta correcta");
                }

                @Override
                public void onFailure(Call<QuestionsResponse> call, Throwable t) {
                    observableBeforeQuestionsControllers.getBnbCallback().bnbError(t.getMessage(), "Error peticion pregunta");
                }
            });
        }
    }
}
