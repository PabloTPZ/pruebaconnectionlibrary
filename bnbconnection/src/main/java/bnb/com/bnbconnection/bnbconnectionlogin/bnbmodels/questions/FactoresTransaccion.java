
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.questions;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FactoresTransaccion {

    @SerializedName("ListaFactores")
    @Expose
    private List<ListaFactore> listaFactores = null;
    @SerializedName("CodigoUsuario")
    @Expose
    private Long codigoUsuario;
    @SerializedName("TienePregunta")
    @Expose
    private Boolean tienePregunta;
    @SerializedName("DesafioPregunta")
    @Expose
    private DesafioPregunta desafioPregunta;
    @SerializedName("TieneTarjeta")
    @Expose
    private Boolean tieneTarjeta;
    @SerializedName("DesafioTarjeta")
    @Expose
    private DesafioTarjeta desafioTarjeta;
    @SerializedName("TieneToken")
    @Expose
    private Boolean tieneToken;
    @SerializedName("DesafioToken")
    @Expose
    private DesafioToken desafioToken;
    @SerializedName("TieneSMS")
    @Expose
    private Boolean tieneSMS;
    @SerializedName("DesafioSMS")
    @Expose
    private DesafioSMS desafioSMS;
    @SerializedName("TieneSoftToken")
    @Expose
    private Boolean tieneSoftToken;
    @SerializedName("DesafioSoftToken")
    @Expose
    private DesafioSoftToken desafioSoftToken;
    @SerializedName("Situacion")
    @Expose
    private Integer situacion;

    public List<ListaFactore> getListaFactores() {
        return listaFactores;
    }

    public void setListaFactores(List<ListaFactore> listaFactores) {
        this.listaFactores = listaFactores;
    }

    public Long getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(Long codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public Boolean getTienePregunta() {
        return tienePregunta;
    }

    public void setTienePregunta(Boolean tienePregunta) {
        this.tienePregunta = tienePregunta;
    }

    public DesafioPregunta getDesafioPregunta() {
        return desafioPregunta;
    }

    public void setDesafioPregunta(DesafioPregunta desafioPregunta) {
        this.desafioPregunta = desafioPregunta;
    }

    public Boolean getTieneTarjeta() {
        return tieneTarjeta;
    }

    public void setTieneTarjeta(Boolean tieneTarjeta) {
        this.tieneTarjeta = tieneTarjeta;
    }

    public DesafioTarjeta getDesafioTarjeta() {
        return desafioTarjeta;
    }

    public void setDesafioTarjeta(DesafioTarjeta desafioTarjeta) {
        this.desafioTarjeta = desafioTarjeta;
    }

    public Boolean getTieneToken() {
        return tieneToken;
    }

    public void setTieneToken(Boolean tieneToken) {
        this.tieneToken = tieneToken;
    }

    public DesafioToken getDesafioToken() {
        return desafioToken;
    }

    public void setDesafioToken(DesafioToken desafioToken) {
        this.desafioToken = desafioToken;
    }

    public Boolean getTieneSMS() {
        return tieneSMS;
    }

    public void setTieneSMS(Boolean tieneSMS) {
        this.tieneSMS = tieneSMS;
    }

    public DesafioSMS getDesafioSMS() {
        return desafioSMS;
    }

    public void setDesafioSMS(DesafioSMS desafioSMS) {
        this.desafioSMS = desafioSMS;
    }

    public Boolean getTieneSoftToken() {
        return tieneSoftToken;
    }

    public void setTieneSoftToken(Boolean tieneSoftToken) {
        this.tieneSoftToken = tieneSoftToken;
    }

    public DesafioSoftToken getDesafioSoftToken() {
        return desafioSoftToken;
    }

    public void setDesafioSoftToken(DesafioSoftToken desafioSoftToken) {
        this.desafioSoftToken = desafioSoftToken;
    }

    public Integer getSituacion() {
        return situacion;
    }

    public void setSituacion(Integer situacion) {
        this.situacion = situacion;
    }

}
