package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionpassword.controllers;

import java.util.Observable;
import java.util.Observer;

import bnb.com.bnbconnection.bnbconnectionlogin.APIServiceLogin;
import bnb.com.bnbconnection.bnbconnectionlogin.BNBConnectionUtils;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionpassword.models.response.LoginResponsePassword;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ObserverPasswordControllers implements Observer {

    private APIServiceLogin apiServiceLogin;
    private ObservablePasswordControllers loginControllers;

    @Override
    public void update(Observable o, Object arg) {
        loginControllers = (ObservablePasswordControllers) o;

        if (loginControllers.getLoginRequestPassword() != null){
            apiServiceLogin = BNBConnectionUtils.getAPIService();

            Call<LoginResponsePassword> call = apiServiceLogin.passwordPost(loginControllers.getLoginRequestPassword());
            call.enqueue(new Callback<LoginResponsePassword>() {
                @Override
                public void onResponse(Call<LoginResponsePassword> call, Response<LoginResponsePassword> response) {
                    if (response.body().getCorrecto()) {
                        loginControllers.getBnbCallback().bnbSuccessful(response.body(), "Clave correcto");
                    } else {
                        loginControllers.getBnbCallback().bnbError(response.body().getMensaje(), "Clave incorrecta");
                    }
                }

                @Override
                public void onFailure(Call<LoginResponsePassword> call, Throwable t) {
                    loginControllers.getBnbCallback().bnbError(t.getMessage(), "Error Petición Clave");
                }
            });
        }
    }

//    public void connection(LoginRequestPassword loginRequestPassword){
//        CompositeDisposable compositeDisposable = new CompositeDisposable();
//        compositeDisposable.add(apiServiceLogin.passwordPost(loginRequestPassword)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<LoginResponsePassword>() {
//                    @Override
//                    public void accept(LoginResponsePassword loginResponseUser) throws Exception {
//                        if (loginResponseUser.getCorrecto()) {
//                            loginControllers.getBnbCallback().bnbSuccessful(loginResponseUser);
//                        } else {
//                            loginControllers.getBnbCallbackPassword().bnbError(loginResponseUser.getMensaje());
//                        }
//                    }
//                }));
//    }
}
