package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionuser.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResquestUser {
    @SerializedName("Origen")
    @Expose
    private String origen;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("Identificador")
    @Expose
    private String identificador;
    @SerializedName("idioma")
    @Expose
    private Integer idioma;
    @SerializedName("TipoDispositivo")
    @Expose
    private Integer tipoDispositivo;
    @SerializedName("Version")
    @Expose
    private Double version;

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public Integer getIdioma() {
        return idioma;
    }

    public void setIdioma(Integer idioma) {
        this.idioma = idioma;
    }

    public Integer getTipoDispositivo() {
        return tipoDispositivo;
    }

    public void setTipoDispositivo(Integer tipoDispositivo) {
        this.tipoDispositivo = tipoDispositivo;
    }

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }
}
