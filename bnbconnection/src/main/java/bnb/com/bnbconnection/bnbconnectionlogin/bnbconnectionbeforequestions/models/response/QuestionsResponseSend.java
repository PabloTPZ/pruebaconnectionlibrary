package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionbeforequestions.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuestionsResponseSend {

    @SerializedName("SituacionFactor")
    @Expose
    private Integer situacionFactor;
    @SerializedName("CambioClaveExitoso")
    @Expose
    private Boolean cambioClaveExitoso;
    @SerializedName("CambioIdentificadorExitoso")
    @Expose
    private Boolean cambioIdentificadorExitoso;
    @SerializedName("RegistroPreguntasExitoso")
    @Expose
    private Boolean registroPreguntasExitoso;
    @SerializedName("RegistrarEquipoExitoso")
    @Expose
    private Boolean registrarEquipoExitoso;
    @SerializedName("Mensaje")
    @Expose
    private String mensaje;
    @SerializedName("Correcto")
    @Expose
    private Boolean correcto;
    @SerializedName("Excepcion")
    @Expose
    private Object excepcion;
    @SerializedName("TipoError")
    @Expose
    private Integer tipoError;

    public Integer getSituacionFactor() {
        return situacionFactor;
    }

    public void setSituacionFactor(Integer situacionFactor) {
        this.situacionFactor = situacionFactor;
    }

    public Boolean getCambioClaveExitoso() {
        return cambioClaveExitoso;
    }

    public void setCambioClaveExitoso(Boolean cambioClaveExitoso) {
        this.cambioClaveExitoso = cambioClaveExitoso;
    }

    public Boolean getCambioIdentificadorExitoso() {
        return cambioIdentificadorExitoso;
    }

    public void setCambioIdentificadorExitoso(Boolean cambioIdentificadorExitoso) {
        this.cambioIdentificadorExitoso = cambioIdentificadorExitoso;
    }

    public Boolean getRegistroPreguntasExitoso() {
        return registroPreguntasExitoso;
    }

    public void setRegistroPreguntasExitoso(Boolean registroPreguntasExitoso) {
        this.registroPreguntasExitoso = registroPreguntasExitoso;
    }

    public Boolean getRegistrarEquipoExitoso() {
        return registrarEquipoExitoso;
    }

    public void setRegistrarEquipoExitoso(Boolean registrarEquipoExitoso) {
        this.registrarEquipoExitoso = registrarEquipoExitoso;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getCorrecto() {
        return correcto;
    }

    public void setCorrecto(Boolean correcto) {
        this.correcto = correcto;
    }

    public Object getExcepcion() {
        return excepcion;
    }

    public void setExcepcion(Object excepcion) {
        this.excepcion = excepcion;
    }

    public Integer getTipoError() {
        return tipoError;
    }

    public void setTipoError(Integer tipoError) {
        this.tipoError = tipoError;
    }

}
