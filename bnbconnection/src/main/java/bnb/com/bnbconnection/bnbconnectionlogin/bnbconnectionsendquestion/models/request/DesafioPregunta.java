
package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DesafioPregunta {

    @SerializedName("Pregunta2")
    @Expose
    private String pregunta2;
    @SerializedName("Respuesta2")
    @Expose
    private String respuesta2;
    @SerializedName("Pregunta3")
    @Expose
    private String pregunta3;
    @SerializedName("Numero1")
    @Expose
    private Integer numero1;
    @SerializedName("Numero3")
    @Expose
    private Integer numero3;
    @SerializedName("SegundaPregunta")
    @Expose
    private Boolean segundaPregunta;
    @SerializedName("PrimerPregunta")
    @Expose
    private Boolean primerPregunta;
    @SerializedName("Respuesta1")
    @Expose
    private String respuesta1;
    @SerializedName("Respuesta3")
    @Expose
    private String respuesta3;
    @SerializedName("Pregunta1")
    @Expose
    private String pregunta1;
    @SerializedName("Numero2")
    @Expose
    private Integer numero2;
    @SerializedName("TerceraPregunta")
    @Expose
    private Boolean terceraPregunta;

    public String getPregunta2() {
        return pregunta2;
    }

    public void setPregunta2(String pregunta2) {
        this.pregunta2 = pregunta2;
    }

    public String getRespuesta2() {
        return respuesta2;
    }

    public void setRespuesta2(String respuesta2) {
        this.respuesta2 = respuesta2;
    }

    public String getPregunta3() {
        return pregunta3;
    }

    public void setPregunta3(String pregunta3) {
        this.pregunta3 = pregunta3;
    }

    public Integer getNumero1() {
        return numero1;
    }

    public void setNumero1(Integer numero1) {
        this.numero1 = numero1;
    }

    public Integer getNumero3() {
        return numero3;
    }

    public void setNumero3(Integer numero3) {
        this.numero3 = numero3;
    }

    public Boolean getSegundaPregunta() {
        return segundaPregunta;
    }

    public void setSegundaPregunta(Boolean segundaPregunta) {
        this.segundaPregunta = segundaPregunta;
    }

    public Boolean getPrimerPregunta() {
        return primerPregunta;
    }

    public void setPrimerPregunta(Boolean primerPregunta) {
        this.primerPregunta = primerPregunta;
    }

    public String getRespuesta1() {
        return respuesta1;
    }

    public void setRespuesta1(String respuesta1) {
        this.respuesta1 = respuesta1;
    }

    public String getRespuesta3() {
        return respuesta3;
    }

    public void setRespuesta3(String respuesta3) {
        this.respuesta3 = respuesta3;
    }

    public String getPregunta1() {
        return pregunta1;
    }

    public void setPregunta1(String pregunta1) {
        this.pregunta1 = pregunta1;
    }

    public Integer getNumero2() {
        return numero2;
    }

    public void setNumero2(Integer numero2) {
        this.numero2 = numero2;
    }

    public Boolean getTerceraPregunta() {
        return terceraPregunta;
    }

    public void setTerceraPregunta(Boolean terceraPregunta) {
        this.terceraPregunta = terceraPregunta;
    }

}
