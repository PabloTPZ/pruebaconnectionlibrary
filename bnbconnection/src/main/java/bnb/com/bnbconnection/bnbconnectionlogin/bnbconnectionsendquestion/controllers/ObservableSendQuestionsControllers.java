package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.controllers;

import java.util.Observable;

import bnb.com.bnbconnection.BNBCallback;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionbeforequestions.models.request.QuestionsSend;

public class ObservableSendQuestionsControllers extends Observable {

    private QuestionsSend questionsSend;
    private BNBCallback bnbCallback;

    public ObservableSendQuestionsControllers(){

    }

    public QuestionsSend getQuestionsSend() {
        return questionsSend;
    }

    public BNBCallback getBnbCallback() {
        return bnbCallback;
    }

    public void setReady(QuestionsSend questionsSend,
                         BNBCallback bnbCallback){
        this.questionsSend = questionsSend;
        this.bnbCallback = bnbCallback;

        setChanged();
        notifyObservers();
    }
}
