
package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionpassword.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cliente {

    @SerializedName("CodigoCliente")
    @Expose
    private Long codigoCliente;
    @SerializedName("Nombre")
    @Expose
    private String nombre;
    @SerializedName("TipoPersona")
    @Expose
    private Integer tipoPersona;
    @SerializedName("Oficina")
    @Expose
    private Integer oficina;
    @SerializedName("Agencia")
    @Expose
    private Integer agencia;

    public Long getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(Long codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(Integer tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public Integer getOficina() {
        return oficina;
    }

    public void setOficina(Integer oficina) {
        this.oficina = oficina;
    }

    public Integer getAgencia() {
        return agencia;
    }

    public void setAgencia(Integer agencia) {
        this.agencia = agencia;
    }

}
