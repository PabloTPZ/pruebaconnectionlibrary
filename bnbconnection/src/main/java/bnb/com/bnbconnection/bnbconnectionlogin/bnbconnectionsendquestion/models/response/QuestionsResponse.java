package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.questions.Factores;

public class QuestionsResponse {
    @SerializedName("Factores")
    @Expose
    private Factores factores;
    @SerializedName("MostrarFactor")
    @Expose
    private Boolean mostrarFactor;
    @SerializedName("TipoError")
    @Expose
    private Integer tipoError;

    public Factores getFactores() {
        return factores;
    }

    public void setFactores(Factores factores) {
        this.factores = factores;
    }

    public Boolean getMostrarFactor() {
        return mostrarFactor;
    }

    public void setMostrarFactor(Boolean mostrarFactor) {
        this.mostrarFactor = mostrarFactor;
    }

    public Integer getTipoError() {
        return tipoError;
    }

    public void setTipoError(Integer tipoError) {
        this.tipoError = tipoError;
    }
}
