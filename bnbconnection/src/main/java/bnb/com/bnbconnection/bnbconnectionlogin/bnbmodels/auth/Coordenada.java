
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.auth;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coordenada {

    @SerializedName("Numero")
    @Expose
    private Integer numero;
    @SerializedName("Desafio")
    @Expose
    private List<Object> desafio = null;

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public List<Object> getDesafio() {
        return desafio;
    }

    public void setDesafio(List<Object> desafio) {
        this.desafio = desafio;
    }

}
