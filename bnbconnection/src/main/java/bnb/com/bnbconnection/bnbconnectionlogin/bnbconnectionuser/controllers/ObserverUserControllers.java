package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionuser.controllers;

import java.util.Observable;
import java.util.Observer;

import bnb.com.bnbconnection.bnbconnectionlogin.APIServiceLogin;
import bnb.com.bnbconnection.bnbconnectionlogin.BNBConnectionUtils;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionuser.models.response.LoginResponseUser;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ObserverUserControllers implements Observer {

    private APIServiceLogin apiServiceLogin;

    @Override
    public void update(Observable o, Object arg) {
        final ObservableUserControllers loginControllers = (ObservableUserControllers) o;

        if (loginControllers.getLoginResquestUser() != null){
            apiServiceLogin = BNBConnectionUtils.getAPIService();

            Call<LoginResponseUser> call = apiServiceLogin.userPost(loginControllers.getLoginResquestUser());
            call.enqueue(new Callback<LoginResponseUser>() {
                @Override
                public void onResponse(Call<LoginResponseUser> call, Response<LoginResponseUser> response) {
                    if (response.body().getCorrecto()) {
                        loginControllers.getBnbCallback().bnbSuccessful(response.body(), "Identificador correcto");
                    } else {
                        loginControllers.getBnbCallback().bnbError(response.body().getMensaje(), "Identificador incorrecto");
                    }
                }

                @Override
                public void onFailure(Call<LoginResponseUser> call, Throwable t) {
                    loginControllers.getBnbCallback().bnbError(t.getMessage(), "Error Petición Identificador");
                }
            });
        }
    }
}
