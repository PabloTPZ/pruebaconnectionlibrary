package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionpassword.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginRequestPassword {

    @SerializedName("Origen")
    @Expose
    private String origen;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("Clave")
    @Expose
    private String clave;
    @SerializedName("Identificador")
    @Expose
    private String identificador;
    @SerializedName("Usuario")
    @Expose
    private Long usuario;

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public Long getUsuario() {
        return usuario;
    }

    public void setUsuario(Long usuario) {
        this.usuario = usuario;
    }

}
