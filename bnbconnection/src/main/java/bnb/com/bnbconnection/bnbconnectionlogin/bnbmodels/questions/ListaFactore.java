
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.questions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListaFactore {

    @SerializedName("Tipo")
    @Expose
    private Integer tipo;
    @SerializedName("Identificador")
    @Expose
    private String identificador;
    @SerializedName("Estado")
    @Expose
    private Integer estado;
    @SerializedName("Nivel")
    @Expose
    private Integer nivel;
    @SerializedName("Intentos")
    @Expose
    private Integer intentos;
    @SerializedName("FechaUltimoBloqueo")
    @Expose
    private String fechaUltimoBloqueo;
    @SerializedName("FechaBloqueo")
    @Expose
    private String fechaBloqueo;

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public Integer getIntentos() {
        return intentos;
    }

    public void setIntentos(Integer intentos) {
        this.intentos = intentos;
    }

    public String getFechaUltimoBloqueo() {
        return fechaUltimoBloqueo;
    }

    public void setFechaUltimoBloqueo(String fechaUltimoBloqueo) {
        this.fechaUltimoBloqueo = fechaUltimoBloqueo;
    }

    public String getFechaBloqueo() {
        return fechaBloqueo;
    }

    public void setFechaBloqueo(String fechaBloqueo) {
        this.fechaBloqueo = fechaBloqueo;
    }

}
