package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.auth.Factore;

public class QuestionsResquest {

    @SerializedName("CodigoUsuario")
    @Expose
    private Long codigoUsuario;
    @SerializedName("Numerotransaccion")
    @Expose
    private Integer numerotransaccion;
    @SerializedName("Origen")
    @Expose
    private String origen;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("Factores")
    @Expose
    private List<Factore> factores = null;
    @SerializedName("CambiarClave")
    @Expose
    private Boolean cambiarClave;
    @SerializedName("CambiarIdentificador")
    @Expose
    private Boolean cambiarIdentificador;
    @SerializedName("CambiarPreguntas")
    @Expose
    private Boolean cambiarPreguntas;
    @SerializedName("RegistrarEquipo")
    @Expose
    private Boolean registrarEquipo;
    @SerializedName("TransaccionNiveles")
    @Expose
    private Object transaccionNiveles;

    public Long getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(Long codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public Integer getNumerotransaccion() {
        return numerotransaccion;
    }

    public void setNumerotransaccion(Integer numerotransaccion) {
        this.numerotransaccion = numerotransaccion;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<Factore> getFactores() {
        return factores;
    }

    public void setFactores(List<Factore> factores) {
        this.factores = factores;
    }

    public Boolean getCambiarClave() {
        return cambiarClave;
    }

    public void setCambiarClave(Boolean cambiarClave) {
        this.cambiarClave = cambiarClave;
    }

    public Boolean getCambiarIdentificador() {
        return cambiarIdentificador;
    }

    public void setCambiarIdentificador(Boolean cambiarIdentificador) {
        this.cambiarIdentificador = cambiarIdentificador;
    }

    public Boolean getCambiarPreguntas() {
        return cambiarPreguntas;
    }

    public void setCambiarPreguntas(Boolean cambiarPreguntas) {
        this.cambiarPreguntas = cambiarPreguntas;
    }

    public Boolean getRegistrarEquipo() {
        return registrarEquipo;
    }

    public void setRegistrarEquipo(Boolean registrarEquipo) {
        this.registrarEquipo = registrarEquipo;
    }

    public Object getTransaccionNiveles() {
        return transaccionNiveles;
    }

    public void setTransaccionNiveles(Object transaccionNiveles) {
        this.transaccionNiveles = transaccionNiveles;
    }

}
