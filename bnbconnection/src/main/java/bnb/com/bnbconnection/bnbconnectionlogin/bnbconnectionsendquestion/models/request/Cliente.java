
package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cliente {

    @SerializedName("Oficina")
    @Expose
    private Integer oficina;
    @SerializedName("Nombre")
    @Expose
    private String nombre;
    @SerializedName("CodigoCliente")
    @Expose
    private Long codigoCliente;
    @SerializedName("Agencia")
    @Expose
    private Integer agencia;
    @SerializedName("TipoPersona")
    @Expose
    private Integer tipoPersona;

    public Integer getOficina() {
        return oficina;
    }

    public void setOficina(Integer oficina) {
        this.oficina = oficina;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(Long codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public Integer getAgencia() {
        return agencia;
    }

    public void setAgencia(Integer agencia) {
        this.agencia = agencia;
    }

    public Integer getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(Integer tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

}
