
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.questions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DesafioPregunta {

    @SerializedName("TipoTransaccion")
    @Expose
    private Integer tipoTransaccion;
    @SerializedName("PrimerPregunta")
    @Expose
    private Boolean primerPregunta;
    @SerializedName("SegundaPregunta")
    @Expose
    private Boolean segundaPregunta;
    @SerializedName("TerceraPregunta")
    @Expose
    private Boolean terceraPregunta;
    @SerializedName("Numero1")
    @Expose
    private Integer numero1;
    @SerializedName("Pregunta1")
    @Expose
    private String pregunta1;
    @SerializedName("Respuesta1")
    @Expose
    private Object respuesta1;
    @SerializedName("Numero2")
    @Expose
    private Integer numero2;
    @SerializedName("Pregunta2")
    @Expose
    private String pregunta2;
    @SerializedName("Respuesta2")
    @Expose
    private Object respuesta2;
    @SerializedName("Numero3")
    @Expose
    private Integer numero3;
    @SerializedName("Pregunta3")
    @Expose
    private String pregunta3;
    @SerializedName("Respuesta3")
    @Expose
    private Object respuesta3;
    @SerializedName("Mensaje")
    @Expose
    private Object mensaje;
    @SerializedName("Correcto")
    @Expose
    private Boolean correcto;

    public Integer getTipoTransaccion() {
        return tipoTransaccion;
    }

    public void setTipoTransaccion(Integer tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    public Boolean getPrimerPregunta() {
        return primerPregunta;
    }

    public void setPrimerPregunta(Boolean primerPregunta) {
        this.primerPregunta = primerPregunta;
    }

    public Boolean getSegundaPregunta() {
        return segundaPregunta;
    }

    public void setSegundaPregunta(Boolean segundaPregunta) {
        this.segundaPregunta = segundaPregunta;
    }

    public Boolean getTerceraPregunta() {
        return terceraPregunta;
    }

    public void setTerceraPregunta(Boolean terceraPregunta) {
        this.terceraPregunta = terceraPregunta;
    }

    public Integer getNumero1() {
        return numero1;
    }

    public void setNumero1(Integer numero1) {
        this.numero1 = numero1;
    }

    public String getPregunta1() {
        return pregunta1;
    }

    public void setPregunta1(String pregunta1) {
        this.pregunta1 = pregunta1;
    }

    public Object getRespuesta1() {
        return respuesta1;
    }

    public void setRespuesta1(Object respuesta1) {
        this.respuesta1 = respuesta1;
    }

    public Integer getNumero2() {
        return numero2;
    }

    public void setNumero2(Integer numero2) {
        this.numero2 = numero2;
    }

    public String getPregunta2() {
        return pregunta2;
    }

    public void setPregunta2(String pregunta2) {
        this.pregunta2 = pregunta2;
    }

    public Object getRespuesta2() {
        return respuesta2;
    }

    public void setRespuesta2(Object respuesta2) {
        this.respuesta2 = respuesta2;
    }

    public Integer getNumero3() {
        return numero3;
    }

    public void setNumero3(Integer numero3) {
        this.numero3 = numero3;
    }

    public String getPregunta3() {
        return pregunta3;
    }

    public void setPregunta3(String pregunta3) {
        this.pregunta3 = pregunta3;
    }

    public Object getRespuesta3() {
        return respuesta3;
    }

    public void setRespuesta3(Object respuesta3) {
        this.respuesta3 = respuesta3;
    }

    public Object getMensaje() {
        return mensaje;
    }

    public void setMensaje(Object mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getCorrecto() {
        return correcto;
    }

    public void setCorrecto(Boolean correcto) {
        this.correcto = correcto;
    }

}
