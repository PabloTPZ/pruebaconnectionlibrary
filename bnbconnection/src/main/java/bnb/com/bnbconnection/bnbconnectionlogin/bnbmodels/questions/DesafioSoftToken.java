
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.questions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DesafioSoftToken {

    @SerializedName("Situacion")
    @Expose
    private Integer situacion;
    @SerializedName("Numero")
    @Expose
    private String numero;
    @SerializedName("TokenSet")
    @Expose
    private String tokenSet;
    @SerializedName("Desafio")
    @Expose
    private Object desafio;
    @SerializedName("Mensaje")
    @Expose
    private Object mensaje;
    @SerializedName("Correcto")
    @Expose
    private Boolean correcto;

    public Integer getSituacion() {
        return situacion;
    }

    public void setSituacion(Integer situacion) {
        this.situacion = situacion;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTokenSet() {
        return tokenSet;
    }

    public void setTokenSet(String tokenSet) {
        this.tokenSet = tokenSet;
    }

    public Object getDesafio() {
        return desafio;
    }

    public void setDesafio(Object desafio) {
        this.desafio = desafio;
    }

    public Object getMensaje() {
        return mensaje;
    }

    public void setMensaje(Object mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getCorrecto() {
        return correcto;
    }

    public void setCorrecto(Boolean correcto) {
        this.correcto = correcto;
    }

}
