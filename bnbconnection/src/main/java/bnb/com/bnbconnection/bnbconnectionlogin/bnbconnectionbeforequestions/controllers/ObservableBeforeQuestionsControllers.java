package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionbeforequestions.controllers;

import java.util.List;
import java.util.Observable;

import bnb.com.bnbconnection.BNBCallback;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.models.request.QuestionsResquest;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.auth.Factore;

public class ObservableBeforeQuestionsControllers extends Observable {

    private QuestionsResquest questionsResquest;
    private List<Factore> factores;
    private BNBCallback bnbCallback;

    public ObservableBeforeQuestionsControllers(){

    }

    public List<Factore> getFactores() {
        return factores;
    }

    public QuestionsResquest getQuestionsResquest() {
        return questionsResquest;
    }

    public BNBCallback getBnbCallback() {
        return bnbCallback;
    }

    public void setReady(QuestionsResquest questionsResquest,
                         List<Factore> factores,
                         BNBCallback bnbCallback){
        this.questionsResquest = questionsResquest;
        this.factores = factores;
        this.bnbCallback = bnbCallback;

        setChanged();
        notifyObservers();
    }
}
