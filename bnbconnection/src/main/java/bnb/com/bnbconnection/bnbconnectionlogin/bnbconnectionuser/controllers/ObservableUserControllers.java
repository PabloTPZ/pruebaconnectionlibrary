package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionuser.controllers;

import java.util.Observable;

import bnb.com.bnbconnection.BNBCallback;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionuser.models.request.LoginResquestUser;

public class ObservableUserControllers extends Observable{

    private LoginResquestUser loginResquestUser;
    private BNBCallback bnbCallback;

    public ObservableUserControllers(){
    }

    public LoginResquestUser getLoginResquestUser() {
        return loginResquestUser;
    }

    public BNBCallback getBnbCallback() {
        return bnbCallback;
    }

    public void setReady(LoginResquestUser loginResquestUser,
                         BNBCallback bnbCallback){
        this.loginResquestUser = loginResquestUser;
        this.bnbCallback = bnbCallback;

        setChanged();
        notifyObservers();
    }
}
