package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionsendquestion.controllers;

import java.util.Observable;
import java.util.Observer;

import bnb.com.bnbconnection.bnbconnectionlogin.APIServiceLogin;
import bnb.com.bnbconnection.bnbconnectionlogin.BNBConnectionUtils;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionbeforequestions.models.response.QuestionsResponseSend;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ObserverSendQuestionsControllers implements Observer {

    private APIServiceLogin apiServiceLogin;

    @Override
    public void update(Observable o, Object arg) {
        final ObservableSendQuestionsControllers observableBeforeQuestionsControllers = (ObservableSendQuestionsControllers) o;

        if (observableBeforeQuestionsControllers.getQuestionsSend() != null){
            apiServiceLogin = BNBConnectionUtils.getAPIService();

            Call<QuestionsResponseSend> call = apiServiceLogin.sendQuestiontPost(observableBeforeQuestionsControllers.getQuestionsSend());
            call.enqueue(new Callback<QuestionsResponseSend>() {
                @Override
                public void onResponse(Call<QuestionsResponseSend> call, Response<QuestionsResponseSend> response) {
                    if (response.body().getCorrecto()){
                        observableBeforeQuestionsControllers.getBnbCallback().bnbSuccessful(response.body(), "Respuesta enviada correctamente");
                    } else {
                        observableBeforeQuestionsControllers.getBnbCallback().bnbError(response.body().getMensaje(), "Respueta enviada incorrecta");
                    }
                }

                @Override
                public void onFailure(Call<QuestionsResponseSend> call, Throwable t) {
                    observableBeforeQuestionsControllers.getBnbCallback().bnbError(t.getMessage(), "Error peticion respuesta");
                }
            });
        }
    }
}
