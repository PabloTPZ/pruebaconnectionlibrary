
package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionuser.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Imagen {

    @SerializedName("Url")
    @Expose
    private String url;
    @SerializedName("UrlCompleta")
    @Expose
    private String urlCompleta;
    @SerializedName("TextoAlternativo")
    @Expose
    private String textoAlternativo;
    @SerializedName("Nombre")
    @Expose
    private String nombre;
    @SerializedName("Tipo")
    @Expose
    private Integer tipo;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlCompleta() {
        return urlCompleta;
    }

    public void setUrlCompleta(String urlCompleta) {
        this.urlCompleta = urlCompleta;
    }

    public String getTextoAlternativo() {
        return textoAlternativo;
    }

    public void setTextoAlternativo(String textoAlternativo) {
        this.textoAlternativo = textoAlternativo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

}
