
package bnb.com.bnbconnection.bnbconnectionlogin.bnbmodels.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Factore {

    @SerializedName("Tipo")
    @Expose
    private Integer tipo;
    @SerializedName("Identificador")
    @Expose
    private String identificador;
    @SerializedName("CodigoPais")
    @Expose
    private Object codigoPais;
    @SerializedName("Estado")
    @Expose
    private Integer estado;
    @SerializedName("Nivel")
    @Expose
    private Integer nivel;
    @SerializedName("Intentos")
    @Expose
    private Integer intentos;
    @SerializedName("FechaUltimoBloqueo")
    @Expose
    private String fechaUltimoBloqueo;
    @SerializedName("FechaBloqueo")
    @Expose
    private String fechaBloqueo;
    @SerializedName("Coordenada")
    @Expose
    private Coordenada coordenada;
    @SerializedName("Pregunta")
    @Expose
    private Pregunta pregunta;
    @SerializedName("Token")
    @Expose
    private Token token;
    @SerializedName("SMS")
    @Expose
    private SMS sMS;
    @SerializedName("SoftToken")
    @Expose
    private SoftToken softToken;

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public Object getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(Object codigoPais) {
        this.codigoPais = codigoPais;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public Integer getIntentos() {
        return intentos;
    }

    public void setIntentos(Integer intentos) {
        this.intentos = intentos;
    }

    public String getFechaUltimoBloqueo() {
        return fechaUltimoBloqueo;
    }

    public void setFechaUltimoBloqueo(String fechaUltimoBloqueo) {
        this.fechaUltimoBloqueo = fechaUltimoBloqueo;
    }

    public String getFechaBloqueo() {
        return fechaBloqueo;
    }

    public void setFechaBloqueo(String fechaBloqueo) {
        this.fechaBloqueo = fechaBloqueo;
    }

    public Coordenada getCoordenada() {
        return coordenada;
    }

    public void setCoordenada(Coordenada coordenada) {
        this.coordenada = coordenada;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public SMS getSMS() {
        return sMS;
    }

    public void setSMS(SMS sMS) {
        this.sMS = sMS;
    }

    public SoftToken getSoftToken() {
        return softToken;
    }

    public void setSoftToken(SoftToken softToken) {
        this.softToken = softToken;
    }

}
