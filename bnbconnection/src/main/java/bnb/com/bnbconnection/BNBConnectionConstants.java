package bnb.com.bnbconnection;

import android.util.Log;

public class BNBConnectionConstants {

    //url servidor de production
//    public static String URLBaseServer = "https://www.bnb.com.bo/bnbnet/api/";
    public static String URLBASESERVERPRUEBA = "http://10.16.22.83/BNBAuthService/api/";
    public static String URLBaseServer = "http://10.16.22.73/bnbnet/api/";
    public static String DATALOGIN = "bnbdata2";
    public static String DATAFACTORES = "factores";
    public static String DATAPASSWORD = "password";
    public static String DATAQUESTIONS = "accept_password";
    public static String ORIGIN = "origin";
    public static String T_OTIGIN = "torigin";
    public static String DATACLAVE = "clave";
    public static String DATAIDEN = "identificador";

    public static void URL( String url){
        BNBConnectionConstants.URLBaseServer = url;
        Log.d("prueba", "URL: " + BNBConnectionConstants.URLBaseServer);
    }
}
