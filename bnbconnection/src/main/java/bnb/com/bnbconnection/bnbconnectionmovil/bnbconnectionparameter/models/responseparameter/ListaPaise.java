package bnb.com.bnbconnection.bnbconnectionmovil.bnbconnectionparameter.models.responseparameter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListaPaise {

    @SerializedName("Pais")
    @Expose
    private String pais;
    @SerializedName("Codigo")
    @Expose
    private Integer codigo;

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

}
