package bnb.com.bnbconnection.bnbconnectionmovil.bnbconnectionparameter.models.responseparameter;

import android.content.Intent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseParameter {
    @SerializedName("ListaPaises")
    @Expose
    private List<ListaPaise> listaPaises = null;
    @SerializedName("NumeroTarjeta")
    @Expose
    private Long numeroTarjeta;
    @SerializedName("FechaInicioHabilitacion")
    @Expose
    private String fechaInicioHabilitacion;
    @SerializedName("FechaFinHabilitacion")
    @Expose
    private String fechaFinHabilitacion;
    @SerializedName("MontoMaximoRetiroCajero")
    @Expose
    private Integer montoMaximoRetiroCajero;
    @SerializedName("CantidadLimiteRetirosCajeros")
    @Expose
    private Integer cantidadLimiteRetirosCajeros;
    @SerializedName("MontoMaximoRetiroComercios")
    @Expose
    private Integer montoMaximoRetiroComercios;
    @SerializedName("CantidadLimiteRetirosComercios")
    @Expose
    private Integer cantidadLimiteRetirosComercios;
    @SerializedName("CuentaSeleccionada")
    @Expose
    private Long cuentaSeleccionada;
    @SerializedName("Cuentas")
    @Expose
    private List<Long> cuentas = null;
    @SerializedName("Mensaje")
    @Expose
    private String mensaje;
    @SerializedName("Correcto")
    @Expose
    private Boolean correcto;
    @SerializedName("Excepcion")
    @Expose
    private Object excepcion;
    @SerializedName("TipoError")
    @Expose
    private Integer tipoError;

    public List<ListaPaise> getListaPaises() {
        return listaPaises;
    }

    public void setListaPaises(List<ListaPaise> listaPaises) {
        this.listaPaises = listaPaises;
    }

    public Long getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(Long numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getFechaInicioHabilitacion() {
        return fechaInicioHabilitacion;
    }

    public void setFechaInicioHabilitacion(String fechaInicioHabilitacion) {
        this.fechaInicioHabilitacion = fechaInicioHabilitacion;
    }

    public String getFechaFinHabilitacion() {
        return fechaFinHabilitacion;
    }

    public void setFechaFinHabilitacion(String fechaFinHabilitacion) {
        this.fechaFinHabilitacion = fechaFinHabilitacion;
    }

    public Integer getMontoMaximoRetiroCajero() {
        return montoMaximoRetiroCajero;
    }

    public void setMontoMaximoRetiroCajero(Integer montoMaximoRetiroCajero) {
        this.montoMaximoRetiroCajero = montoMaximoRetiroCajero;
    }

    public Integer getCantidadLimiteRetirosCajeros() {
        return cantidadLimiteRetirosCajeros;
    }

    public void setCantidadLimiteRetirosCajeros(Integer cantidadLimiteRetirosCajeros) {
        this.cantidadLimiteRetirosCajeros = cantidadLimiteRetirosCajeros;
    }

    public Integer getMontoMaximoRetiroComercios() {
        return montoMaximoRetiroComercios;
    }

    public void setMontoMaximoRetiroComercios(Integer montoMaximoRetiroComercios) {
        this.montoMaximoRetiroComercios = montoMaximoRetiroComercios;
    }

    public Integer getCantidadLimiteRetirosComercios() {
        return cantidadLimiteRetirosComercios;
    }

    public void setCantidadLimiteRetirosComercios(Integer cantidadLimiteRetirosComercios) {
        this.cantidadLimiteRetirosComercios = cantidadLimiteRetirosComercios;
    }

    public Long getCuentaSeleccionada() {
        return cuentaSeleccionada;
    }

    public void setCuentaSeleccionada(Long cuentaSeleccionada) {
        this.cuentaSeleccionada = cuentaSeleccionada;
    }

    public List<Long> getCuentas() {
        return cuentas;
    }

    public void setCuentas(List<Long> cuentas) {
        this.cuentas = cuentas;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getCorrecto() {
        return correcto;
    }

    public void setCorrecto(Boolean correcto) {
        this.correcto = correcto;
    }

    public Object getExcepcion() {
        return excepcion;
    }

    public void setExcepcion(Object excepcion) {
        this.excepcion = excepcion;
    }

    public Integer getTipoError() {
        return tipoError;
    }

    public void setTipoError(Integer tipoError) {
        this.tipoError = tipoError;
    }
}
