package bnb.com.bnbconnection.bnbconnectionmovil.bnbconnectionparameter.controllers.bnbconnectionsendparametercontrollers;

import java.util.Observable;

import bnb.com.bnbconnection.BNBCallback;
import bnb.com.bnbconnection.bnbconnectionmovil.bnbconnectionparameter.models.sendparameter.SendParameter;

public class ObservableSendParameterControllers extends Observable {

    private SendParameter sendParameter;
    private BNBCallback bnbCallback;

    public ObservableSendParameterControllers(){

    }

    public SendParameter getSendParameter() {
        return sendParameter;
    }

    public BNBCallback getBnbCallback() {
        return bnbCallback;
    }

    public void setReady(SendParameter sendParameter,
                         BNBCallback bnbCallback){
        this.sendParameter = sendParameter;
        this.bnbCallback = bnbCallback;

        setChanged();
        notifyObservers();
    }
}
