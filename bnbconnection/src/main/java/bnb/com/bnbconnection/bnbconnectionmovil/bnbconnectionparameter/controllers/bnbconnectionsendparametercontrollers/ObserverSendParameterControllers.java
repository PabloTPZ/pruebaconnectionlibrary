package bnb.com.bnbconnection.bnbconnectionmovil.bnbconnectionparameter.controllers.bnbconnectionsendparametercontrollers;

import java.util.Observable;
import java.util.Observer;

import bnb.com.bnbconnection.bnbconnectionmovil.APIServiceParameter;
import bnb.com.bnbconnection.bnbconnectionmovil.BNBConnectionUtilsMovil;
import bnb.com.bnbconnection.bnbconnectionmovil.bnbconnectionparameter.models.responseparameter.ResponseParameter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ObserverSendParameterControllers implements Observer {

    private APIServiceParameter apiServiceParameter;
    private ObservableSendParameterControllers parameterControllers;

    @Override
    public void update(Observable observable, Object o) {
        parameterControllers = (ObservableSendParameterControllers) observable;

        if (parameterControllers.getSendParameter() != null) {
            apiServiceParameter = BNBConnectionUtilsMovil.getAPIServiceMovil();

            Call<ResponseParameter> call = apiServiceParameter.sendParameter(parameterControllers.getSendParameter());
            call.enqueue(new Callback<ResponseParameter>() {
                @Override
                public void onResponse(Call<ResponseParameter> call, Response<ResponseParameter> response) {
                    if (response.body().getCorrecto()) {
                        parameterControllers.getBnbCallback().bnbSuccessful(response.body(), "parametros correcto");
                    } else {
                        parameterControllers.getBnbCallback().bnbError(response.body().getMensaje(), "parametros incorrecta");
                    }
                }

                @Override
                public void onFailure(Call<ResponseParameter> call, Throwable t) {
                    parameterControllers.getBnbCallback().bnbError(t.getMessage(), "Error Petición parametros");
                }
            });
        }

    }
}
