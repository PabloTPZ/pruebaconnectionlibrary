package bnb.com.bnbconnection.bnbconnectionmovil;
import bnb.com.bnbconnection.BNBConnectionConstants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BNBConnectionUtilsMovil {

    private static Retrofit retrofit = null;

    //retrofit
    public static APIServiceParameter getAPIServiceMovil() {
        return getClient(BNBConnectionConstants.URLBaseServer).create(APIServiceParameter.class);
    }

    private static Retrofit getClient(String baseURL){

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        return retrofit;
    }
}
