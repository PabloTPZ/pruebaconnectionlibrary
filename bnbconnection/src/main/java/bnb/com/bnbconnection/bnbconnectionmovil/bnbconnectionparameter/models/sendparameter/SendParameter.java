package bnb.com.bnbconnection.bnbconnectionmovil.bnbconnectionparameter.models.sendparameter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendParameter {

    @SerializedName("Origen")
    @Expose
    private String origen;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("NroTarjeta")
    @Expose
    private Long nroTarjeta;
    @SerializedName("TipoOrigen")
    @Expose
    private Integer tipoOrigen;
    @SerializedName("Identificador")
    @Expose
    private String identificador;
    @SerializedName("Clave")
    @Expose
    private String clave;
    @SerializedName("NroClienteSeleccionado")
    @Expose
    private Long nroClienteSeleccionado;

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getNroTarjeta() {
        return nroTarjeta;
    }

    public void setNroTarjeta(Long nroTarjeta) {
        this.nroTarjeta = nroTarjeta;
    }

    public Integer getTipoOrigen() {
        return tipoOrigen;
    }

    public void setTipoOrigen(Integer tipoOrigen) {
        this.tipoOrigen = tipoOrigen;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Long getNroClienteSeleccionado() {
        return nroClienteSeleccionado;
    }

    public void setNroClienteSeleccionado(Long nroClienteSeleccionado) {
        this.nroClienteSeleccionado = nroClienteSeleccionado;
    }

}
