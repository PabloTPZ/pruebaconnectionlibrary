package bnb.com.bnbconnection.bnbconnectionmovil;

import bnb.com.bnbconnection.bnbconnectionmovil.bnbconnectionparameter.models.responseparameter.ResponseParameter;
import bnb.com.bnbconnection.bnbconnectionmovil.bnbconnectionparameter.models.sendparameter.SendParameter;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIServiceParameter {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("ParametrosMobile/TraerParametros")
    Call<ResponseParameter> sendParameter(@Body SendParameter body);

}
