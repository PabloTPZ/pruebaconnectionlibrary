package bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.controllers.bnbconnectionotp;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import bnb.com.bnbconnection.bnbconnectionauthentication.APIServiceAuthentication;
import bnb.com.bnbconnection.bnbconnectionauthentication.BNBConnectionUtilsAuthentication;
import bnb.com.bnbconnection.bnbconnectionlogin.APIServiceLogin;
import bnb.com.bnbconnection.bnbconnectionlogin.BNBConnectionUtils;
import bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.models.Response.ResponseOTP;
import bnb.com.bnbconnection.bnbconnectionauthentication.bnbconnectiondatahotp.models.SendDataOTP;
import retrofit2.Call;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OTPTest {

    @Mock
    APIServiceAuthentication apiServiceLogin;
    SendDataOTP sendDataOTP;
    ResponseOTP responseOTP;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        apiServiceLogin = BNBConnectionUtilsAuthentication.getAPIServiceOTAPIServiceAuthentication();

        sendDataOTP = new SendDataOTP();
        responseOTP = new ResponseOTP();
    }

    @Test
    public void OTPPetition() throws IOException {

        sendDataOTP.setUserId("46481900001");
        sendDataOTP.setDeviceId("39f21d747f7373d1-unknown");

        Call<ResponseOTP> responseOTPCall = apiServiceLogin.sendDataOTP(sendDataOTP);
        responseOTP = responseOTPCall.execute().body();

        assertEquals("Correcto.", responseOTP.getMessage());
        assertTrue(responseOTP.getSuccess());
    }
}