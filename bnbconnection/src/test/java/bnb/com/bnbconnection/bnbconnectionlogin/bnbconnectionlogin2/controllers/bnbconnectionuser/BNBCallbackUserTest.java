package bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionlogin2.controllers.bnbconnectionuser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import bnb.com.bnbconnection.bnbconnectionlogin.APIServiceLogin;
import bnb.com.bnbconnection.bnbconnectionlogin.BNBConnectionUtils;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionpassword.models.request.LoginRequestPassword;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionpassword.models.response.LoginResponsePassword;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionuser.models.response.LoginResponseUser;
import bnb.com.bnbconnection.bnbconnectionlogin.bnbconnectionuser.models.request.LoginResquestUser;
import retrofit2.Call;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class BNBCallbackUserTest {

    @Mock
    APIServiceLogin apiServiceLogin;

    LoginResquestUser loginResquest;
    LoginRequestPassword loginRequestPassword;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        apiServiceLogin = BNBConnectionUtils.getAPIService();

        loginResquest = new LoginResquestUser();
        loginRequestPassword = new LoginRequestPassword();
    }

    @Test
    public void bnbSuccessful() throws InterruptedException, IOException {
        loginResquest.setOrigen("39f21d747f7373d1-unknown");
        loginResquest.setVersion(3.4);
        loginResquest.setToken("B7C9432E663E07C5953F6CD62552D7C97D386F7FD1DB54BBF7190AC95181C4FEBF14859E76D6BE6E861AAC4D22D88EFA");
        loginResquest.setTipoDispositivo(4);
        loginResquest.setIdioma(1);
        loginResquest.setIdentificador("dongato1590");

        Call<LoginResponseUser> observable = apiServiceLogin.userPost(loginResquest);
        String number = "4648190000";
        Long user = Long.parseLong(number);
        LoginResponseUser loginResponseUser = observable.execute().body();
        assertEquals(user, loginResponseUser.getUsuario());
        assertTrue(loginResponseUser.getCorrecto());
    }

    @Test
    public void bnbError() throws IOException, InterruptedException {
        loginRequestPassword.setOrigen("39f21d747f7373d1-unknown");
        loginRequestPassword.setToken("D52883A7B097DAA8341C42E660022AA86615EA269BADC7988E50159B69212C24BB5DA3139BB6DA6DD81FFBCDBAA2A678");
        loginRequestPassword.setClave("1990Dongao");
        loginRequestPassword.setIdentificador("dongato1590");
        String number = "46481900001";
        Long user = Long.parseLong(number);
        loginRequestPassword.setUsuario(user);

        Call<LoginResponsePassword> responsePasswordCall = apiServiceLogin.passwordPost(loginRequestPassword);
        LoginResponsePassword loginRequestPassword = responsePasswordCall.execute().body();
        Thread.sleep(2000);
        if (loginRequestPassword.getMensaje().equals("14Acceso NO Permitido"))
            assertTrue(true);
        else if (loginRequestPassword.getMensaje().equals("14Acceso Bloqueado por Hoy"))
            assertTrue(true);
        else if (loginRequestPassword.getMensaje().equals(""))
            assertTrue(true);
        else
            assertTrue(false);
    }
}